from django.core.management.base import BaseCommand, CommandError

from market.models import Category, Product

from backend.settings import DATA_DIR

from openpyxl import load_workbook


class Command(BaseCommand):

    def handle(self, *args, **options):
        print('Clearing DB')
        Category.objects.all().delete()
        Product.objects.all().delete()

        print('Start importing from excel %s' % DATA_DIR)
        wb = load_workbook(DATA_DIR+'/price.xlsx')
        worksheet = wb.get_sheet_by_name(wb.get_sheet_names()[0])
        cat = None
        for cnt in range(1, worksheet.max_row+1):
            item = worksheet.cell(row=cnt, column=3).value
            id = worksheet.cell(row=cnt, column=2).value
            if id == None:
                print('Create a new category')
                cat = Category()
                cat.name = item
                cat.save()
            else:
                print('Create a new goods')
                if cat:
                    product = Product()
                    product.name = item
                    product.category = cat
                    product.save()

    