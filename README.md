# FoodDelivery to your home

## Clone the project

    git clone https://gitlab.com/VOlni/fooddeliverybackend/
    cd fooddeliverybackend

## Project deployment

    ./deploy

## Run backend server

    cd backend
    ./bin/run